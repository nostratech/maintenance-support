package com.nostra.maintenance.support.maintenancesupport.common.enums;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Operation of search criteria
 */
public enum SearchCriteriaOperation {
    EQUALITY("~"), NEGATION("!"), GREATER_THAN(">"), GREATER_THAN_OR_EQUAL(">="), LESS_THAN("<"), LESS_THAN_OR_EQUAL("<="), STARTS_WITH(null), ENDS_WITH(null), CONTAINS(null);
    public static final Collection<String> SIMPLE_OPERATION_SET = Collections.unmodifiableList(
            Arrays.asList(
                    EQUALITY.toString(),NEGATION.toString(),GREATER_THAN_OR_EQUAL.toString(),
                    GREATER_THAN.toString(),LESS_THAN_OR_EQUAL.toString(),LESS_THAN.toString()
            )
    );
    public static final String WILDCARD = "*";

    SearchCriteriaOperation(String operation) {
        this.operation = operation;
    }

    private final String operation;

    /**
     * Get operation from input
     *
     * @param input string representation of operation
     * @return search criteria operation
     */
    public static SearchCriteriaOperation getSimpleOperation(final String input) {
        switch (input) {
            case "~":
                return EQUALITY;
            case "!":
                return NEGATION;
            case ">":
                return GREATER_THAN;
            case ">=":
                return GREATER_THAN_OR_EQUAL;
            case "<":
                return LESS_THAN;
            case "<=":
                return LESS_THAN_OR_EQUAL;
            default:
                return null;
        }
    }

    /**
     * Check if argument has wildcard (true) or not
     *
     * @param fix suffix or prefix
     * @return
     */
    public static Boolean hasWildcard(String fix) {
        return fix.contains(SearchCriteriaOperation.WILDCARD);
    }

    @Override
    public String toString() {
        return operation;
    }
}
