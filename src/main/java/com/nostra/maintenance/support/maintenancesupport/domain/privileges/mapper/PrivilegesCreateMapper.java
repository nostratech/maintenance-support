package com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.BaseMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PrivilegesCreateMapper extends BaseMapper<Privileges, PrivilegesCreateRequestDTO> {

    PrivilegesCreateMapper INSTANCE = Mappers.getMapper(PrivilegesCreateMapper.class);

    PrivilegesCreateResponseDTO toResponse(Privileges entity);

}
