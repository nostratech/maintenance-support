package com.nostra.maintenance.support.maintenancesupport.controller;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("api/v1/roles")
public class RolesController {

    private RolesService rolesService;

    @PostMapping
    public ResponseEntity<Void> create(@RequestBody RolesCreateRequestDTO request) {
        RolesCreateResponseDTO result = rolesService.create(request);
        return ResponseEntity.created(URI.create("v1/principals/" + result.getSecureId())).build();
    }

    @GetMapping
    public ResponseEntity<PageDTO<RolesGetResponseDTO>> get(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit,
            @RequestParam(value = "sort", required = false, defaultValue = "") Collection<String> sort,
            @RequestParam(value = "search", required = false, defaultValue = "") String search
    ) {
        PageDTO<RolesGetResponseDTO> result = rolesService.get(page, limit, sort, search);

        return ResponseEntity.ok(result);
    }
}
