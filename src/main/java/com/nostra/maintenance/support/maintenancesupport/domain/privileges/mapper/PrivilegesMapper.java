package com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.BaseMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PrivilegesMapper extends BaseMapper<Privileges, PrivilegesDTO> {

    PrivilegesMapper INSTANCE = Mappers.getMapper(PrivilegesMapper.class);

}
