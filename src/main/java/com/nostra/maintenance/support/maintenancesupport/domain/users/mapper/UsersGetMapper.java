package com.nostra.maintenance.support.maintenancesupport.domain.users.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.UsersGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.users.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UsersGetMapper {
    UsersGetMapper INSTANCE = Mappers.getMapper(UsersGetMapper.class);
    UsersGetResponseDTO toDTO(Users entity);
}
