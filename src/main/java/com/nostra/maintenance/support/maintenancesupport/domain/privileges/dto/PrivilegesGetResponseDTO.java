package com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.BaseResponseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class PrivilegesGetResponseDTO extends BaseResponseDTO {

    private String authority;
    private String description;

}
