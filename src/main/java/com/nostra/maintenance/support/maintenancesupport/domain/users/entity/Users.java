package com.nostra.maintenance.support.maintenancesupport.domain.users.entity;

import com.nostra.maintenance.support.maintenancesupport.domain.base.entity.BaseEntity;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Entity(name = "users")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Users extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 346727426154616546L;

    @Column(name = "username", unique = true)
    @NotNull(message = "Username is mandatory")
    @NotBlank(message = "Username must not blank")
    private String username;

    @Column(name = "password")
    @NotNull(message = "Password is mandatory")
    @NotBlank(message = "Password must not blank")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT),  // currently hibernate does not generate multiple cascade on M-N table creation, so must be manually create on sql script
            inverseForeignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT)  // idem
    )
    @NotEmpty(message = "Role is mandatory")
    private Set<Roles> roles;

    @Column(name = "is_enable")
    private Boolean isEnable;
}
