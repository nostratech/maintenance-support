package com.nostra.maintenance.support.maintenancesupport.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.zalando.problem.AbstractThrowableProblem;

/**
 * Thrown when procedure is not implemented
 */
@ResponseStatus(HttpStatus.NOT_IMPLEMENTED)
public class NotImplementedException extends AbstractThrowableProblem {}
