package com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl;

import com.nostra.maintenance.support.maintenancesupport.common.enums.Description;
import com.nostra.maintenance.support.maintenancesupport.common.enums.SearchCriteriaOperation;
import com.nostra.maintenance.support.maintenancesupport.common.enums.SearchQueryOperation;
import com.nostra.maintenance.support.maintenancesupport.common.exception.BadRequestMessageException;
import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.SearchCriteriaDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.base.entity.BaseEntity;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.SearchQueryMapper;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of search query mapper
 */
public class SearchQueryMapperImpl<T extends BaseEntity> implements SearchQueryMapper<T> {

    private static final String QUERY_OPERATOR_NAME = "query operator";
    private static final String ENUM_CONSTANT_NAME = "enum constant";

    public static <T extends BaseEntity> Specification<T> of(String query) {
        SearchQueryMapper<T> searchQueryMapper = new SearchQueryMapperImpl<>();

        return searchQueryMapper.toPredicate(query);
    }

    /**
     * Map query to predicate of entity
     *
     * @param query query to map
     * @return predicate
     */
    public Specification<T> toPredicate(String query) {
        List<SearchCriteriaDTO> searchCriteria = parse(query);

        if(!searchCriteria.isEmpty()) {
            SearchCriteriaDTO firstElement = searchCriteria.remove(0);
            Specification<T> specification = toPredicate(firstElement);
            SearchQueryOperation currentQueryOperation = firstElement.getQueryOperation();

            for (SearchCriteriaDTO searchCriterion : searchCriteria) {
                if (specification != null && SearchQueryOperation.OR.equals(currentQueryOperation)) {
                    specification = specification.or(toPredicate(searchCriterion));
                } else if (specification != null) {
                    specification = specification.and(toPredicate(searchCriterion));
                }

                currentQueryOperation = searchCriterion.getQueryOperation();
            }

            return Optional.ofNullable(specification)
                    .orElseThrow(() -> new BadRequestMessageException("query param is not valid", null, null));
        } else {
            return null;
        }
    }

    /**
     * Map query to predicate of entity
     *
     * @param criteria criteria to map
     * @return predicate
     *
     * @see SearchCriteriaDTO
     *
     * @throws BadRequestMessageException if criteria given is not valid or does not exist
     */
    public Specification<T> toPredicate(SearchCriteriaDTO criteria) {

        return (root, query, criteriaBuilder) -> {
            String property = Optional.of(criteria.getProperty()).orElse("");
            String value = Optional.of(criteria.getValue()).map(this::normalizeValue).orElse("");

            try {
                Path<?> parsedPropertyPath = parseProperty(root, property);
                if(Objects.equals(parsedPropertyPath.getJavaType(), Date.class)) {
                    Path<Date> pathProperty = (Path<Date>) parsedPropertyPath;
                    Date pathValue = Date.from(ZonedDateTime.parse(value).toLocalDateTime().atZone(ZoneId.systemDefault()).toInstant());

                    return toComparablePredicate(criteriaBuilder,pathProperty,criteria.getOperation(),pathValue);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), Short.class)) {
                    Path<Short> pathProperty = (Path<Short>) parsedPropertyPath;
                    Short pathValue = Short.parseShort(value);

                    return toComparablePredicate(criteriaBuilder,pathProperty,criteria.getOperation(),pathValue);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), Integer.class)) {
                    Path<Integer> pathProperty = (Path<Integer>) parsedPropertyPath;
                    Integer pathValue = Integer.parseInt(value);

                    return toComparablePredicate(criteriaBuilder, pathProperty, criteria.getOperation(), pathValue);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), Float.class)) {
                    Path<Float> pathProperty = (Path<Float>) parsedPropertyPath;
                    Float pathValue = Float.parseFloat(value);

                    return toComparablePredicate(criteriaBuilder, pathProperty, criteria.getOperation(), pathValue);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), Double.class)) {
                    Path<Double> pathProperty = (Path<Double>) parsedPropertyPath;
                    Double pathValue = Double.parseDouble(value);

                    return toComparablePredicate(criteriaBuilder, pathProperty, criteria.getOperation(), pathValue);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), BigInteger.class)) {
                    Path<BigInteger> pathProperty = (Path<BigInteger>) parsedPropertyPath;
                    BigInteger pathValue = BigInteger.valueOf(Long.valueOf(value));

                    return toComparablePredicate(criteriaBuilder, pathProperty, criteria.getOperation(), pathValue);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), BigDecimal.class)) {
                    Path<BigDecimal> pathProperty = (Path<BigDecimal>) parsedPropertyPath;
                    BigDecimal pathValue = BigDecimal.valueOf(Double.valueOf(value));

                    return toComparablePredicate(criteriaBuilder, pathProperty, criteria.getOperation(), pathValue);
                } else if(parsedPropertyPath.getJavaType() != null && parsedPropertyPath.getJavaType().isEnum()) {
                    Path<Enum<?>> pathProperty = (Path<Enum<?>>) parsedPropertyPath;

                    return toEnumPredicate(criteriaBuilder, pathProperty, criteria.getOperation(), value);
                } else if(Objects.equals(parsedPropertyPath.getJavaType(), String.class)) {
                    Path<String> pathProperty = (Path<String>) parsedPropertyPath;

                    return toStringPredicate(criteriaBuilder,pathProperty,criteria.getOperation(),value);
                } else {
                    throw new BadRequestMessageException("property of" + property + "has invalid query value", value, null);
                }
            } catch (IllegalArgumentException e) {
                throw new BadRequestMessageException("property of " + property + " does not exist", property, null);
            }
        };
    }

    // parse string query to SearchCriteriaDTO
    private List<SearchCriteriaDTO> parse(String query) {
        Pattern pattern = Pattern.compile(
                "([\\w.]+?)(" +
                String.join("|", SearchCriteriaOperation.SIMPLE_OPERATION_SET) +
                ")(\\" + SearchCriteriaOperation.WILDCARD + "?)([\\w\\W]+?)(\\" + SearchCriteriaOperation.WILDCARD + "?)(" +
                String.join("|", SearchQueryOperation.SIMPLE_OPERATION_SET) +
                ")"
        );
        Matcher matcher = pattern.matcher(query + SearchQueryOperation.AND.toString());
        List<SearchCriteriaDTO> result = new ArrayList<>();
        while (matcher.find()) {
            result.add(SearchCriteriaDTO.of(matcher.group(1), matcher.group(2), matcher.group(4), matcher.group(3), matcher.group(5), matcher.group(6)));
        }

        return result;
    }

    // parse string property to predicate path
    private <Y> Path<Y> parseProperty(Root<T> root, String property) {
        Deque<String> properties = new ArrayDeque<>(Arrays.asList(property.split("\\.")));
        Path<Y> rootPath = root.get(properties.pop());

        while(!properties.isEmpty()) {
            rootPath = rootPath.get(properties.pop());
        }

        return rootPath;
    }

    private <Y extends Comparable<? super Y>> Predicate toComparablePredicate(CriteriaBuilder criteriaBuilder, Path<Y> pathProperty, SearchCriteriaOperation operation, Y pathValue) {
        if(SearchCriteriaOperation.EQUALITY.equals(operation)) {
            return criteriaBuilder.equal(pathProperty, pathValue);
        } else if(SearchCriteriaOperation.NEGATION.equals(operation)) {
            return criteriaBuilder.notEqual(pathProperty, pathValue);
        } else if(SearchCriteriaOperation.GREATER_THAN.equals(operation)) {
            return criteriaBuilder.greaterThan(pathProperty, pathValue);
        } else if(SearchCriteriaOperation.GREATER_THAN_OR_EQUAL.equals(operation)) {
            return criteriaBuilder.greaterThanOrEqualTo(pathProperty, pathValue);
        } else if(SearchCriteriaOperation.LESS_THAN.equals(operation)) {
            return criteriaBuilder.lessThan(pathProperty, pathValue);
        } else if(SearchCriteriaOperation.LESS_THAN_OR_EQUAL.equals(operation)) {
            return criteriaBuilder.lessThanOrEqualTo(pathProperty, pathValue);
        } else if(SearchCriteriaOperation.ENDS_WITH.equals(operation)) {
            return criteriaBuilder.or(
                    criteriaBuilder.lessThan(pathProperty, pathValue),
                    criteriaBuilder.isNull(pathProperty)
            );
        } else if(SearchCriteriaOperation.STARTS_WITH.equals(operation)) {
            return criteriaBuilder.or(
                    criteriaBuilder.greaterThan(pathProperty, pathValue),
                    criteriaBuilder.isNull(pathProperty)
            );
        }

        throw new BadRequestMessageException(Description.NOT_VALID.toString(QUERY_OPERATOR_NAME), pathProperty.toString(), null);
    }

    private Predicate toStringPredicate(CriteriaBuilder criteriaBuilder, Path<String> pathProperty, SearchCriteriaOperation operation, String pathValue) {
        if(SearchCriteriaOperation.EQUALITY.equals(operation)) {
            return criteriaBuilder.equal(criteriaBuilder.lower(pathProperty), pathValue.toLowerCase());
        } else if(SearchCriteriaOperation.NEGATION.equals(operation)) {
            return criteriaBuilder.notEqual(criteriaBuilder.lower(pathProperty), pathValue.toLowerCase());
        } else if(SearchCriteriaOperation.GREATER_THAN.equals(operation)) {
            return criteriaBuilder.greaterThan(criteriaBuilder.lower(pathProperty), pathValue.toLowerCase());
        } else if(SearchCriteriaOperation.LESS_THAN.equals(operation)) {
            return criteriaBuilder.lessThan(criteriaBuilder.lower(pathProperty), pathValue.toLowerCase());
        } else if(SearchCriteriaOperation.STARTS_WITH.equals(operation)) {
            return criteriaBuilder.like(criteriaBuilder.lower(pathProperty), "%" + pathValue.toLowerCase());
        } else if(SearchCriteriaOperation.ENDS_WITH.equals(operation)) {
            return criteriaBuilder.like(criteriaBuilder.lower(pathProperty), pathValue.toLowerCase() + "%");
        } else if(SearchCriteriaOperation.CONTAINS.equals(operation)) {
            return criteriaBuilder.like(criteriaBuilder.lower(pathProperty), "%" + pathValue.toLowerCase() + "%");
        }

        throw new BadRequestMessageException(Description.NOT_VALID.toString(QUERY_OPERATOR_NAME), pathProperty.toString(), null);
    }

    private Predicate toEnumPredicate(CriteriaBuilder criteriaBuilder, Path<Enum<?>> pathProperty, SearchCriteriaOperation operation, String pathValue) {
        Enum<?> filteredEnum = Arrays.stream(pathProperty.getJavaType().getEnumConstants())
                .filter( cons -> cons.name().equalsIgnoreCase(pathValue))
                .findFirst()
                .orElseThrow(
                        () -> new BadRequestMessageException(Description.NOT_VALID.toString(ENUM_CONSTANT_NAME), pathValue, null)
                );

        if(SearchCriteriaOperation.EQUALITY.equals(operation)) {
            return criteriaBuilder.equal(pathProperty, filteredEnum);
        } else if(SearchCriteriaOperation.NEGATION.equals(operation)) {
            return criteriaBuilder.notEqual(pathProperty, filteredEnum);
        }

        throw new BadRequestMessageException(Description.NOT_VALID.toString(QUERY_OPERATOR_NAME), pathProperty.toString(), null);
    }

    private String normalizeValue(String value) {
        // normalize value to search so it compliment with database query
        if(null == value) {
            return null;
        }

        return value.replace("%", "\\%").replace("_", "\\_");
    }

}
