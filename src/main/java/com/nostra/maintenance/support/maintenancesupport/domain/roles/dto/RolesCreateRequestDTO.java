package com.nostra.maintenance.support.maintenancesupport.domain.roles.dto;

import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesGetResponseDTO;
import lombok.Data;

import java.util.Collection;

@Data
public class RolesCreateRequestDTO {
    private String name;

    private Collection<PrivilegesGetResponseDTO> privileges;
}
