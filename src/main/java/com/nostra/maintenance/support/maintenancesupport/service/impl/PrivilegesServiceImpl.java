package com.nostra.maintenance.support.maintenancesupport.service.impl;

import com.nostra.maintenance.support.maintenancesupport.common.enums.Description;
import com.nostra.maintenance.support.maintenancesupport.common.exception.DataNotFoundException;
import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl.PageMapperImpl;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl.SearchQueryMapperImpl;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesUpdateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper.PrivilegesCreateMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper.PrivilegesGetMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper.PrivilegesUpdateMapper;
import com.nostra.maintenance.support.maintenancesupport.repository.PrivilegesRepository;
import com.nostra.maintenance.support.maintenancesupport.service.PrivilegesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class PrivilegesServiceImpl implements PrivilegesService {

    private final PrivilegesRepository privilegesRepository;

    @Autowired
    public PrivilegesServiceImpl(PrivilegesRepository privilegesRepository) {
        this.privilegesRepository = privilegesRepository;
    }

    @Override
    public PrivilegesCreateResponseDTO create(PrivilegesCreateRequestDTO request) {
        Privileges privilege = PrivilegesCreateMapper.INSTANCE.toEntity(request);

        Privileges result = privilegesRepository
                .save(privilege);

        return PrivilegesCreateMapper.INSTANCE.toResponse(result);
    }

    @Override
    public void delete(String id) {
        privilegesRepository.deleteBySecureId(id);
    }

    @Override
    public PageDTO<PrivilegesGetResponseDTO> get(Integer page, Integer limit, Collection<String> sorts, String search) {
        Page<Privileges> result = privilegesRepository
                .findAll(SearchQueryMapperImpl.of(search), PageMapperImpl.constructPageable(page, limit, sorts));

        return PageMapperImpl.constructResponse(
                result.getContent().stream().map(PrivilegesGetMapper.INSTANCE::toDTO).collect(Collectors.toList()),
                page,
                result.getTotalPages(),
                result.getTotalElements()
        );
    }

    @Override
    public PrivilegesGetResponseDTO get(String id) {
        return privilegesRepository
                .findBySecureId(id)
                .map(PrivilegesGetMapper.INSTANCE::toDTO)
                .orElseThrow(() -> new DataNotFoundException(
                                Description.DATA_NOT_FOUND.toString(Privileges.class.getSimpleName().toLowerCase()), Privileges.class.getSimpleName().toLowerCase(), null
                        )
                );
    }

    @Override
    public PrivilegesGetResponseDTO getByDesc(String description) {
        Privileges privileges = privilegesRepository.findByDescription(description);

        return PrivilegesGetMapper.INSTANCE.toDTO(privileges);
    }

    @Override
    public void update(String id, PrivilegesUpdateRequestDTO request) {
        Privileges privilege = privilegesRepository
                .findBySecureId(id)
                .orElseThrow(() -> new DataNotFoundException(Description.DATA_NOT_FOUND.toString(Privileges.class.getSimpleName().toLowerCase()), Privileges.class.getSimpleName().toLowerCase(), null));

        privilege = PrivilegesUpdateMapper.INSTANCE.toDomain(request, privilege);

        privilegesRepository
                .save(privilege);
    }
}
