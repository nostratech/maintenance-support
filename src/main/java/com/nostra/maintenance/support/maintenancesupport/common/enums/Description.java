package com.nostra.maintenance.support.maintenancesupport.common.enums;

/**
 * Enum of description with formatting
 */
public enum Description {

    // Resource
    DATA_NOT_FOUND("Data %s is not found"),

    // Validation
    NOT_VALID("%s is not valid");

    private final String descriptionFormat;

    /**
     * Create new enum
     *
     * @param descriptionFormat format of error description
     */
    Description(String descriptionFormat) {
        this.descriptionFormat = descriptionFormat;
    }

    /**
     * Formatting with String.format based on arguments
     *
     * @param args argument to format
     * @return formatted string
     */
    public String toString(Object... args) {
        return String.format(descriptionFormat, args);
    }
}
