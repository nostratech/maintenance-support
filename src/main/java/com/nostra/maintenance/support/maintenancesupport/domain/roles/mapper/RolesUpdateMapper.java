package com.nostra.maintenance.support.maintenancesupport.domain.roles.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesUpdateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesUpdateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper
public interface RolesUpdateMapper {
    RolesUpdateMapper INSTANCE = Mappers.getMapper(RolesUpdateMapper.class);

    RolesUpdateResponseDTO toVo(Roles entity);

    @Mapping(source = "requestDTO.name",target = "name")
    @Mapping(source = "requestDTO.privileges", target = "privileges")
    Roles toDomain(Roles entity, RolesUpdateRequestDTO requestDTO);
}
