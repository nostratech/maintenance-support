package com.nostra.maintenance.support.maintenancesupport.domain.roles.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.BaseMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RolesCreateMapper extends BaseMapper<Roles, RolesCreateRequestDTO> {

    RolesCreateMapper INSTANCE = Mappers.getMapper(RolesCreateMapper.class);

    RolesCreateResponseDTO toResponse(Roles entity);

}
