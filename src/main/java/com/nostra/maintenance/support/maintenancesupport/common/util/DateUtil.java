package com.nostra.maintenance.support.maintenancesupport.common.util;

import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Slf4j
public class DateUtil {

    private DateUtil(){}

    public static final String DATE_TIME_PATTERN_NO_SPACE = "ddMMYYYYHHmmss";
    public static final String DATE_TIME_PATTERN = "dd-MM-YYYY HH:mm:ss";

    public static String formatDateToString(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        return simpleDateFormat.format(date);
    }

    public static String formatZoneDateTimeToString(ZonedDateTime date, String pattern) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(pattern);
        return date.format(dateTimeFormatter);
    }

    public static Date formatStringToDate(String date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return null;
    }
}
