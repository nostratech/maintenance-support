package com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.BaseMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesUpdateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PrivilegesUpdateMapper extends BaseMapper<Privileges, PrivilegesUpdateRequestDTO> {

    PrivilegesUpdateMapper INSTANCE = Mappers.getMapper(PrivilegesUpdateMapper.class);

    @Mapping(source = "dto.authority", target = "authority")
    @Mapping(source = "dto.description", target = "description")
    Privileges toDomain(PrivilegesUpdateRequestDTO dto, Privileges domain);

}
