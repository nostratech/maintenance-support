package com.nostra.maintenance.support.maintenancesupport.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown resource is not found
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class DataNotFoundException extends BaseException {

    public DataNotFoundException(String defaultMessage, String entityName, String errorKey) {
        super(defaultMessage, entityName, errorKey);
    }
}
