package com.nostra.maintenance.support.maintenancesupport.service.impl;

import com.nostra.maintenance.support.maintenancesupport.common.enums.Description;
import com.nostra.maintenance.support.maintenancesupport.common.exception.DataNotFoundException;
import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl.PageMapperImpl;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl.SearchQueryMapperImpl;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.*;
import com.nostra.maintenance.support.maintenancesupport.domain.users.entity.Users;
import com.nostra.maintenance.support.maintenancesupport.domain.users.mapper.UsersCreateMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.users.mapper.UsersGetMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.users.mapper.UsersUpdateMapper;
import com.nostra.maintenance.support.maintenancesupport.repository.RolesRepository;
import com.nostra.maintenance.support.maintenancesupport.repository.UsersRepository;
import com.nostra.maintenance.support.maintenancesupport.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final RolesRepository roleRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository, RolesRepository roleRepository) {
        this.usersRepository = usersRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public UsersCreateResponseDTO create(UsersCreateRequestDTO request) {
        Set<Roles> role = request.getRole().stream().map(
                roleDTO -> findRoleById(roleDTO.getSecureId())).collect(Collectors.toSet());
        Users entity = UsersCreateMapper.INSTANCE.toEntity(request);
        entity.setRoles(role);

        Users result = usersRepository.save(entity);

        return UsersCreateMapper.INSTANCE.toDTO(result);
    }

    @Override
    public void delete(String id) {
        usersRepository.deleteBySecureId(id);
    }

    @Override
    public UsersUpdateResponseDTO update(String id, UsersUpdateRequestDTO request) {
        Users entity = usersRepository.findBySecureId(id).
                orElseThrow(() -> new DataNotFoundException(Description.DATA_NOT_FOUND.toString(Users.class.getSimpleName().toLowerCase()), null, null));
        entity = UsersUpdateMapper.INSTANCE.toEntity(entity, request);

        Set<Roles> role = request.getRole().stream().map(
                roleDTO -> findRoleById(roleDTO.getSecureId())).collect(Collectors.toSet());
        entity.setRoles(role);

        entity = usersRepository.save(entity);

        return UsersUpdateMapper.INSTANCE.toDTO(entity);
    }

    @Override
    public PageDTO<UsersGetResponseDTO> get(Integer page, Integer limit, Collection<String> sorts, String search) {
        Page<Users> result = usersRepository
                .findAll(SearchQueryMapperImpl.of(search), PageMapperImpl.constructPageable(page, limit, sorts));

        return PageMapperImpl.constructResponse(
                result.getContent().stream().map(UsersGetMapper.INSTANCE::toDTO).collect(Collectors.toList()),
                page,
                result.getTotalPages(),
                result.getTotalElements()
        );
    }

    @Override
    public UsersGetResponseDTO get(String id) {
        return usersRepository.findBySecureId(id)
                .map(UsersGetMapper.INSTANCE::toDTO)
                .orElseThrow(()-> new DataNotFoundException(Description.DATA_NOT_FOUND.toString(Users.class.getSimpleName().toLowerCase()), null, null));
    }

    private Roles findRoleById(String id) {
        return roleRepository.findBySecureId(id).orElseThrow(() ->
                new DataNotFoundException(Description.DATA_NOT_FOUND.toString(Roles.class.getSimpleName().toLowerCase()), null, null));
    }
}
