package com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.BaseMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PrivilegesGetMapper extends BaseMapper<Privileges, PrivilegesGetResponseDTO> {

    PrivilegesGetMapper INSTANCE = Mappers.getMapper(PrivilegesGetMapper.class);

}
