package com.nostra.maintenance.support.maintenancesupport.domain.users.dto;

import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesGetResponseDTO;
import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class UsersUpdateRequestDTO implements Serializable {
    private static final long serialVersionUID = -5735195130176489408L;
    private String username;
    private String password;
    private Set<RolesGetResponseDTO> role;
    private Boolean isEnable;
}
