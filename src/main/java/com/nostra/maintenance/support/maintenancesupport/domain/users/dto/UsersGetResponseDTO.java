package com.nostra.maintenance.support.maintenancesupport.domain.users.dto;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.BaseResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesGetResponseDTO;
import lombok.Data;

import java.util.Set;

@Data
public class UsersGetResponseDTO extends BaseResponseDTO {
    private String username;
    private String password;
    private Set<RolesGetResponseDTO> role;
    private Boolean isEnable;
}
