package com.nostra.maintenance.support.maintenancesupport.domain.base.dto;

import lombok.Data;

import java.util.Collection;

@Data
public class PageDTO <T> {

    public PageDTO(Collection<T> data, Integer currentPage, Integer totalPages, Long totalElements) {
        this.data = data;
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }

    private Collection<T> data;
    private Integer currentPage;
    private Integer totalPages;
    private Long totalElements;

}
