package com.nostra.maintenance.support.maintenancesupport.controller;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesUpdateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.service.PrivilegesService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping("api/v1/privileges")
public class PrivilegesController {
    private final PrivilegesService privilegesService;

    public PrivilegesController(PrivilegesService privilegesService) {
        this.privilegesService = privilegesService;
    }

    @PostMapping
    public ResponseEntity<PrivilegesCreateResponseDTO> create(@RequestBody PrivilegesCreateRequestDTO request) {
        PrivilegesCreateResponseDTO result = privilegesService.create(request);

        return ResponseEntity.created(URI.create("api/v1/privilege/" + result.getSecureId())).body(result);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        privilegesService.delete(id);

        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<PageDTO<PrivilegesGetResponseDTO>> get(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "limit", required = false, defaultValue = "10") Integer limit,
            @RequestParam(value = "sort", required = false, defaultValue = "") Collection<String> sort,
            @RequestParam(value = "search", required = false, defaultValue = "") String search
    ) {
        PageDTO<PrivilegesGetResponseDTO> result = privilegesService.get(page, limit, sort, search);

        return ResponseEntity.ok(result);
    }

    @GetMapping("{id}")
    public ResponseEntity<PrivilegesGetResponseDTO> get(@PathVariable(value = "id") String id) {
        PrivilegesGetResponseDTO result =privilegesService.get(id);
        return ResponseEntity.ok(result);
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> update(@PathVariable(name = "id") String id, @RequestBody PrivilegesUpdateRequestDTO request) {
        privilegesService.update(id, request);

        return ResponseEntity.noContent().build();
    }

    @GetMapping("/getByDesc/{desc}") 
    public ResponseEntity<PrivilegesGetResponseDTO> getByDesc(@PathVariable(value = "desc") String desc) {
        PrivilegesGetResponseDTO result = privilegesService.getByDesc(desc);
        return ResponseEntity.ok(result);
    }

    
    @GetMapping("/getByRolePrivileges")
    public ResponseEntity<PrivilegesGetResponseDTO> cantBeCastObject() {

        PageDTO<PrivilegesGetResponseDTO> result = null;
        try
        {
            result = privilegesService.get(0, 10, null, "rolePrivileges");
        }
        catch(NullPointerException e)
        {
            e.printStackTrace();
        }
        return (ResponseEntity<PrivilegesGetResponseDTO>) ResponseEntity.ok();
    }

}
