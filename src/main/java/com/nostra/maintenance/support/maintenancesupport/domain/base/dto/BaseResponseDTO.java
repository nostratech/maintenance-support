package com.nostra.maintenance.support.maintenancesupport.domain.base.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public abstract class BaseResponseDTO implements Serializable {
    private static final long serialVersionUID = 4479594253809301204L;

    @JsonProperty("id")
    protected String secureId;
    private ZonedDateTime createdDate;
    private ZonedDateTime modifiedDate;

    @Override
    public int hashCode() {
        return secureId.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        BaseResponseDTO that = (BaseResponseDTO) obj;
        return Objects.equals(secureId, that.secureId);
    }
}
