package com.nostra.maintenance.support.maintenancesupport.repository;

import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends BaseRepository<Roles, Long>{
}
