package com.nostra.maintenance.support.maintenancesupport.service;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.*;

import java.util.Collection;

public interface UsersService {
    UsersCreateResponseDTO create(UsersCreateRequestDTO request);
    void delete(String id);
    UsersUpdateResponseDTO update(String id, UsersUpdateRequestDTO request);
    PageDTO<UsersGetResponseDTO> get(Integer page, Integer limit, Collection<String> sorts, String search);
    UsersGetResponseDTO get(String id);
}
