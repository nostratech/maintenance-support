package com.nostra.maintenance.support.maintenancesupport.domain.roles.entity;

import com.nostra.maintenance.support.maintenancesupport.domain.base.entity.BaseEntity;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

@Entity(name = "roles")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Roles extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -7437818256920447707L;

    @Column(name = "name")
    @NotNull(message = "Name is mandatory")
    @NotBlank(message = "Name must not blank")
    private String name;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(
            name = "roles_privileges",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "privilege_id"),
            foreignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT),  // currently hibernate does not generate multiple cascade on M-N table creation, so must be manually create on sql script
            inverseForeignKey = @ForeignKey(ConstraintMode.NO_CONSTRAINT)  // idem
    )
    private Set<Privileges> privileges;
}
