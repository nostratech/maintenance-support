package com.nostra.maintenance.support.maintenancesupport.repository;

import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrivilegesRepository extends BaseRepository<Privileges, Long> {
    Privileges findByDescription(String description);
}
