package com.nostra.maintenance.support.maintenancesupport.common.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ErrorCode {

    GLOBAL(2),

    INTERNAL_ERROR(12), AUTHENTICATION(10), AUTHORIZATION(11), BAD_REQUEST(13);

    private int errCode;

    private ErrorCode(int errorCode) {
        this.errCode = errorCode;
    }

    @JsonValue
    public int getErrorCode() {
        return errCode;
    }
}
