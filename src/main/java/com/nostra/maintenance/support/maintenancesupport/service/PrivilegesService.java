package com.nostra.maintenance.support.maintenancesupport.service;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesUpdateRequestDTO;

import java.util.Collection;

public interface PrivilegesService {
    PrivilegesCreateResponseDTO create(PrivilegesCreateRequestDTO request);
    void delete(String id);
    PageDTO<PrivilegesGetResponseDTO> get(Integer page, Integer limit, Collection<String> sorts, String search);
    PrivilegesGetResponseDTO get(String id);
    PrivilegesGetResponseDTO getByDesc(String description);
    void update(String id, PrivilegesUpdateRequestDTO request);
}
