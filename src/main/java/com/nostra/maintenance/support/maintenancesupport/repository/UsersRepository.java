package com.nostra.maintenance.support.maintenancesupport.repository;

import com.nostra.maintenance.support.maintenancesupport.domain.users.entity.Users;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends BaseRepository<Users, Long> {
}
