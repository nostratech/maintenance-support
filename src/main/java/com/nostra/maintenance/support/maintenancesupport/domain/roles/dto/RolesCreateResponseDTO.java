package com.nostra.maintenance.support.maintenancesupport.domain.roles.dto;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.BaseResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto.PrivilegesGetResponseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Collection;

@Data
@EqualsAndHashCode(callSuper = true)
public class RolesCreateResponseDTO extends BaseResponseDTO {
    private String name;

    private Collection<PrivilegesGetResponseDTO> privileges;
}
