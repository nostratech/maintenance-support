package com.nostra.maintenance.support.maintenancesupport.common.enums;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Operation query between criteria
 */
public enum SearchQueryOperation {
    AND("_AND_"), OR("_OR_");

    SearchQueryOperation(String operation) {
        this.operation = operation;
    }

    private final String operation;

    public static final Collection<String> SIMPLE_OPERATION_SET = Collections.unmodifiableList(Arrays.asList("(" + AND + ")","(" + OR + ")"));
    /**
     * Get operation from input
     *
     * @param input string representation of operation
     * @return search criteria operation
     */
    public static SearchQueryOperation getSimpleOperation(final String input) {
        switch (input) {
            case "_AND_":
                return AND;
            case "_OR_":
                return OR;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        return operation;
    }
}
