package com.nostra.maintenance.support.maintenancesupport.domain.privileges.dto;

import lombok.Data;

@Data
public class PrivilegesCreateRequestDTO {

    private String authority;
    private String description;

}
