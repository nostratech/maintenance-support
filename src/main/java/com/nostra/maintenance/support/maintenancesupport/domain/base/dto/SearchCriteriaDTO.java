package com.nostra.maintenance.support.maintenancesupport.domain.base.dto;

import com.nostra.maintenance.support.maintenancesupport.common.enums.SearchCriteriaOperation;
import com.nostra.maintenance.support.maintenancesupport.common.enums.SearchQueryOperation;
import lombok.Data;

/**
 * DTO of search query criteria
 */
@Data
public class SearchCriteriaDTO {

    public SearchCriteriaDTO(){}

    public SearchCriteriaDTO(String property, SearchCriteriaOperation operation, String value, SearchQueryOperation queryOperation) {
        this.property = property;
        this.operation = operation;
        this.value = value;
        this.queryOperation = queryOperation;
    }

    private String property;
    private SearchCriteriaOperation operation;
    private String value;
    private SearchQueryOperation queryOperation;

    @Override
    public String toString() {
        return String.format("%s%s%s%s", property,operation.toString(),value,queryOperation.toString());
    }

    /**
     * Construct search criteria DTO from parameters
     *
     * @return SearchCriteriaDTO
     */
    public static SearchCriteriaDTO of(String property, String operation, String value, String prefix, String suffix, String query) {
        SearchCriteriaOperation criteriaOperation = SearchCriteriaOperation.getSimpleOperation(operation);
        SearchQueryOperation queryOperation = SearchQueryOperation.getSimpleOperation(query);
        SearchCriteriaDTO result = new SearchCriteriaDTO(property, criteriaOperation, value, queryOperation);

        boolean hasPrefix = prefix != null && SearchCriteriaOperation.hasWildcard(prefix);
        boolean hasSuffix = suffix != null && SearchCriteriaOperation.hasWildcard(suffix);

        if(criteriaOperation != null && hasPrefix && hasSuffix) {
            result.setOperation(SearchCriteriaOperation.CONTAINS);
        } else if(criteriaOperation != null && hasPrefix) {
            result.setOperation(SearchCriteriaOperation.STARTS_WITH);
        } else if(criteriaOperation != null && hasSuffix) {
            result.setOperation(SearchCriteriaOperation.ENDS_WITH);
        }

        return result;
    }

}
