package com.nostra.maintenance.support.maintenancesupport.domain.base.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.SearchCriteriaDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.base.entity.BaseEntity;
import org.springframework.data.jpa.domain.Specification;

public interface SearchQueryMapper<T extends BaseEntity> {

    Specification<T> toPredicate(String query);
    Specification<T> toPredicate(SearchCriteriaDTO criteria);

}
