package com.nostra.maintenance.support.maintenancesupport.domain.base.mapper;

import java.util.List;

public interface BaseMapper<D, V> {

    D toEntity(V v);
    V toDTO(D d);

    List<D> toEntitys(List<V> v);
    List<V> toDTOS(List<D> d);
}
