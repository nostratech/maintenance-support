package com.nostra.maintenance.support.maintenancesupport.domain.users.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.UsersCreateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.UsersCreateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.users.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UsersCreateMapper {
    UsersCreateMapper INSTANCE = Mappers.getMapper(UsersCreateMapper.class);

    Users toEntity(UsersCreateRequestDTO requestDTO);

    UsersCreateResponseDTO toDTO(Users entity);
}
