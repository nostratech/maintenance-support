package com.nostra.maintenance.support.maintenancesupport.domain.users.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.UsersUpdateRequestDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.users.dto.UsersUpdateResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.users.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper
public interface UsersUpdateMapper {
    UsersUpdateMapper INSTANCE = Mappers.getMapper(UsersUpdateMapper.class);

    @Mapping(source = "requestDTO.username", target = "username")
    @Mapping(source = "requestDTO.password", target = "password")
    @Mapping(source = "requestDTO.isEnable", target = "isEnable")
    Users toEntity(Users entity, UsersUpdateRequestDTO requestDTO);

    UsersUpdateResponseDTO toDTO(Users entity);
}
