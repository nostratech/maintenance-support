package com.nostra.maintenance.support.maintenancesupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class MaintenanceSupportApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MaintenanceSupportApiApplication.class, args);
	}

}
