package com.nostra.maintenance.support.maintenancesupport.service;


import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.*;

import java.util.Collection;

public interface RolesService {

    RolesCreateResponseDTO create(RolesCreateRequestDTO request);
    void delete(String id);
    RolesUpdateResponseDTO update(String id, RolesUpdateRequestDTO request);
    PageDTO<RolesGetResponseDTO> get(Integer page, Integer limit, Collection<String> sorts, String search);
    RolesGetResponseDTO get(String id);
}
