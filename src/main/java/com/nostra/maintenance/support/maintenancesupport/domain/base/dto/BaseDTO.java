package com.nostra.maintenance.support.maintenancesupport.domain.base.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public abstract class BaseDTO implements Serializable {

    private static final long serialVersionUID = 4545185671907397146L;
    private Long id;

    private String secureId;

    private Date createdDate;

    private Date modifiedDate;

    private String createdBy;

    private String modifiedBy;


    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        BaseDTO that = (BaseDTO) obj;
        return Objects.equals(id, that.id) &&
                Objects.equals(secureId, that.secureId);
    }
}
