package com.nostra.maintenance.support.maintenancesupport.service.impl;

import com.nostra.maintenance.support.maintenancesupport.common.enums.Description;
import com.nostra.maintenance.support.maintenancesupport.common.exception.DataNotFoundException;
import com.nostra.maintenance.support.maintenancesupport.domain.base.dto.PageDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl.PageMapperImpl;
import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.impl.SearchQueryMapperImpl;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity.Privileges;
import com.nostra.maintenance.support.maintenancesupport.domain.privileges.mapper.PrivilegesGetMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.*;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.mapper.RolesCreateMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.mapper.RolesGetMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.mapper.RolesUpdateMapper;
import com.nostra.maintenance.support.maintenancesupport.repository.RolesRepository;
import com.nostra.maintenance.support.maintenancesupport.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RolesServiceImpl implements RolesService {

    private final RolesRepository roleRepository;

    @Autowired
    public RolesServiceImpl(RolesRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public RolesCreateResponseDTO create(RolesCreateRequestDTO request) {
        Set<Privileges> privileges = request.getPrivileges().stream()
                .map(PrivilegesGetMapper.INSTANCE::toEntity).collect(Collectors.toSet());
        Roles entity = RolesCreateMapper.INSTANCE.toEntity(request);
        entity.setPrivileges(privileges);

        Roles result = roleRepository.save(entity);

        return RolesCreateMapper.INSTANCE.toResponse(result);
    }

    @Override
    public void delete(String id) {

    }

    @Override
    public RolesUpdateResponseDTO update(String id, RolesUpdateRequestDTO request) {
        Roles entity = roleRepository.findBySecureId(id).
                orElseThrow(() -> new DataNotFoundException(Description.DATA_NOT_FOUND.toString(Roles.class.getSimpleName().toLowerCase()), null, null));
        entity = RolesUpdateMapper.INSTANCE.toDomain(entity, request);

        Set<Privileges> privileges = request.getPrivileges().stream().
                map(PrivilegesGetMapper.INSTANCE::toEntity).collect(Collectors.toSet());
        entity.setPrivileges(privileges);

        entity = roleRepository.save(entity);

        return RolesUpdateMapper.INSTANCE.toVo(entity);
    }

    @Override
    public PageDTO<RolesGetResponseDTO> get(Integer page, Integer limit, Collection<String> sorts, String search) {
        Page<Roles> result = roleRepository
                .findAll(SearchQueryMapperImpl.of(search), PageMapperImpl.constructPageable(page, limit, sorts));

        return PageMapperImpl.constructResponse(
                result.getContent().stream().map(RolesGetMapper.INSTANCE::toDTO).collect(Collectors.toList()),
                page,
                result.getTotalPages(),
                result.getTotalElements()
        );
    }

    @Override
    public RolesGetResponseDTO get(String id) {
        return roleRepository.findBySecureId(id)
                .map(RolesGetMapper.INSTANCE::toDTO)
                .orElseThrow(()-> new DataNotFoundException(Description.DATA_NOT_FOUND.toString(Roles.class.getSimpleName().toLowerCase()), null, null));
    }
}
