package com.nostra.maintenance.support.maintenancesupport.domain.privileges.entity;

import com.nostra.maintenance.support.maintenancesupport.domain.base.entity.BaseEntity;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity(name = "privileges")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Privileges extends BaseEntity implements Serializable {
    private static final long serialVersionUID = -3254769957077829799L;

    @Column(name = "authority", unique = true)
    @NotNull(message = "Authority is mandatory")
    @NotBlank(message = "Authority must not blank")
    private String authority;

    private String description;
}
