package com.nostra.maintenance.support.maintenancesupport.repository;

import com.nostra.maintenance.support.maintenancesupport.domain.base.entity.BaseEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Collection;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<T extends BaseEntity, ID> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

    Optional<T> findBySecureId(String id);
    Collection<T> findBySecureIdIn(Collection<String> id);
    void deleteBySecureId(String id);
}
