package com.nostra.maintenance.support.maintenancesupport.domain.roles.mapper;

import com.nostra.maintenance.support.maintenancesupport.domain.base.mapper.BaseMapper;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.dto.RolesGetResponseDTO;
import com.nostra.maintenance.support.maintenancesupport.domain.roles.entity.Roles;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface RolesGetMapper extends BaseMapper<Roles, RolesGetResponseDTO> {

    RolesGetMapper INSTANCE = Mappers.getMapper(RolesGetMapper.class);
    
}
